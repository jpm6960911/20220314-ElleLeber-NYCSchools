//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            NavCoordinatorView()
        }
    }
}
