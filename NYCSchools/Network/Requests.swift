//
//  Requests.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation

struct AllSchoolRequest: RequestType {
    typealias Response = [School]
    
    let path: String = "/resource/s3k6-pzi2.json"
    
    var queryParams: [URLQueryItem] {
        [.init(name: "$select", value: "school_name,dbn,total_students,overview_paragraph,primary_address_line_1,city,zip,state_code,school_email,phone_number,fax_number,website,latitude,longitude")]
    }
}

struct SATRequest: RequestType {
    typealias Response = [SATScore]
    
    let dbn: String
    init(dbn: String) {
        self.dbn = dbn
    }
    
    var path: String {
        "/resource/f9bf-2cp4.json"
    }

    var queryParams: [URLQueryItem] {
        [.init(name: "dbn", value: dbn)]
    }
}
