//
//  AppNetwork.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation

public enum NetworkError: Error {
    case notHTTPResponse
    case httpError(code: Int)
}

class AppNetwork {
    let session: URLSession
    let baseUrl: URL
    let decoder: JSONDecoder
    
    init(session: URLSession = .shared, baseUrl: URL, decoder: JSONDecoder) {
        self.session = session
        self.baseUrl = baseUrl
        self.decoder = decoder
    }
    
    func perform<Request: RequestType>(request: Request) async throws -> Request.Response {
        do {
            let urlRequest = try request.toUrlRequest(baseUrl: baseUrl)
            print("OUTGOING REQUEST:")
            print("\(String(describing: urlRequest.url))")
            let (data, response) = try await session.data(for: urlRequest)
            print("RESPONSE:")
            print("\(response)")
            // uncomment to log the data blob, this gets kind of large
//            print("response body: \(data.base64EncodedString())")
            guard let httpResponse = response as? HTTPURLResponse else {
                throw NetworkError.notHTTPResponse
            }
            switch httpResponse.statusCode {
            case 200...299:
                // 200 response
                return try decoder.decode(Request.Response.self, from: data)
            default:
                throw NetworkError.httpError(code: httpResponse.statusCode)
            }
        } catch {
            print("Error occurred while requesting: \(error)")
            print("localized description: \(error.localizedDescription)")
            throw error
        }
    }
}

protocol RequestType {
    associatedtype Response: Decodable
    
    var queryParams: [URLQueryItem] { get }
    var path: String { get }
    func toUrlRequest(baseUrl: URL) throws -> URLRequest
}

extension RequestType {
    var queryParams: [URLQueryItem] {
        return []
    }
    
    func toUrlRequest(baseUrl: URL) throws -> URLRequest {
        let url = baseUrl.appending(path: path).appending(queryItems: queryParams)
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue(Constants.APIKey, forHTTPHeaderField: "X-App-Token")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        return request
    }
}
