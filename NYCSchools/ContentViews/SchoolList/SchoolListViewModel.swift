//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation
class SchoolListViewModel: ObservableObject {
    enum State {
        case idle
        case fetching
        case success([School])
        case error(Error)
    }
    
    @Published var state: State = .idle
    let service: NYSchoolDataServiceType
    
    init(service: NYSchoolDataServiceType = NYSchoolDataService()) {
        self.service = service
    }
    
    @MainActor
    func initialFetch() async {
        guard case .idle = state else {
            return
        }
        await fetch()
    }
    
    @MainActor
    func retry() async {
        await fetch()
    }
    
    @MainActor
    private func fetch() async {
        do {
            state = .fetching
            let schools = try await service.getAllSchools()
            state = .success(schools)
        } catch {
            state = .error(error)
        }
    }
}
