//
//  SchoolRowView.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation
import SwiftUI

struct SchoolRowView: View {
    let school: School
    
    var body: some View {
        VStack(spacing: 4) {
            HStack {
                Text(school.schoolName)
                    .bold()
                Spacer()
            }
            HStack {
                Text("\(school.totalStudents) enrolled.")
                Spacer()
            }
        } // VStack
    } // body
}
