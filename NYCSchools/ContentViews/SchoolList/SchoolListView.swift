//
//  SchoolListView.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation
import SwiftUI

struct SchoolListView: View {
    @StateObject var viewModel: SchoolListViewModel = .init()
    
    var body: some View {
        VStack(spacing: 0) {
            switch viewModel.state {
            case .idle:
                idleView()
            case .fetching:
                loadingView()
            case .success(let schools):
                resultsView(schools)
            case .error(let error):
                errorView(error)
            }
        } // VStack
        .navigationTitle("NY Schools")
        .task {
            await viewModel.initialFetch()
        }
    } // body
    
    @ViewBuilder
    private func idleView() -> some View {
        EmptyView()
    }
    
    @ViewBuilder
    private func loadingView() -> some View {
        VStack(spacing: 0) {
            Spacer()
            Text("Loading...")
            Spacer()
        }
    }
    
    @ViewBuilder
    private func resultsView(_ schools: [School]) -> some View {
        List(schools) { school in
            NavigationLink(value: HomeRoute.school(school)) {
                SchoolRowView(school: school)
            }
            .listRowBackground(Color.clear)
        }
        .listStyle(.plain)
    }
    
    @ViewBuilder
    private func errorView(_ error: Error) -> some View {
        VStack(spacing: 0) {
            Spacer()
            Text("Error loading data, please try again.")
            Button("RETRY") {
                Task {
                    await viewModel.retry()
                }
            }
            Spacer()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView(viewModel: .init())
    }
}
