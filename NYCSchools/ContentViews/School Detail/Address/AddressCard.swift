//
//  AddressCard.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation
import SwiftUI

struct AddressCard: View {
    @Environment(\.openURL) var openURL

    let school: School
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text("Address")
                .bold()
                .padding(.bottom, 4)
            Divider()
                .background(.gray)
                .padding(.bottom, 8)
            HStack {
                VStack(alignment: .leading) {
                    Text(school.schoolName)
                    Text(school.primaryAddressLine1)
                    HStack {
                        Text(school.city) + Text(", ") + Text(school.stateCode)
                    }
                    Text(school.zip)
                } // VStack
                Spacer()
                if let latitude = school.latitude,
                   let longitude = school.longitude {
                    Button {
                        // launch map
                        let mapUrl = URL(string: "maps://?saddr=&daddr=\(latitude),\(longitude)")
                        openURL(mapUrl!)
                    } label: {
                        Image(systemName: "arrow.triangle.turn.up.right.diamond")
                            .font(.system(size: 32))
                    }
                }
            } // HStack
        } // VStack
    } // body
}
