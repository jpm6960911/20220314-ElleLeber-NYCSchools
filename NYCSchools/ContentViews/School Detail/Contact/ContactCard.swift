//
//  ContactCard.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation
import SwiftUI

struct ContactCard: View {
    let school: School

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text("Contact")
                .bold()
                .padding(.bottom, 4)
            Divider()
                .background(.gray)
                .padding(.bottom, 8)
            if let email = school.schoolEmail {
                Text(markdown: "Email: [\(email)](mailto:\(email))")
            }
            Text(markdown: "Phone: [\(school.phoneNumber)](tel:\(school.phoneNumber))")
            if let faxNumber = school.faxNumber {
                Text(markdown: "Fax: [\(faxNumber)](tel:\(faxNumber))")
            }
            if let websiteUrl = school.websiteUrl {
                HStack(spacing: 0) {
                    Text(markdown: "Website: [\(school.website)](\(websiteUrl))")
                }
                .multilineTextAlignment(.leading)
            }
        } // VStack
    } // body
}
