//
//  SchoolDetailView.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation
import SwiftUI

struct SchoolDetailView: View {
    @ObservedObject var viewModel: SchoolDetailViewModel
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            Text(viewModel.school.overviewParagraph)
                .padding(.top, 8)
            HStack {
                Text("Enrolled")
                HorizontalLineDivider()
                Text("\(viewModel.school.totalStudents) students")
            }
            .padding(.bottom, 16)
            HStack {
                AddressCard(school: viewModel.school)
                Spacer()
            }
            .padding(.bottom, 16)
            HStack {
                ContactCard(school: viewModel.school)
                Spacer()
            }
            .padding(.bottom, 16)
            HStack {
                SATCardView(viewModel: .init(dbn: viewModel.school.dbn))
            } // HStack
        } // ScrollView
        .padding(.horizontal, 16)
        .navigationBarTitleDisplayMode(.inline)
    } // body
}

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let school = School(schoolName: "Test School", dbn: "1234", totalStudents: "123", overviewParagraph: "the quick brown fox jumped over the lazy dog. the quick brown fox jumped over the lazy dog. the quick brown fox jumped over the lazy dog. the quick brown fox jumped over the lazy dog. the quick brown fox jumped over the lazy dog. ", primaryAddressLine1: "123 Sesame St", city: "New York", zip: "11223", stateCode: "NY", schoolEmail: "bigbird@sesamestreet.com", phoneNumber: "999-123-4567", faxNumber: "999-222-4567", website: "https://sesamestreet.com", latitude: "", longitude: "")
        SchoolDetailView(viewModel: .init(school: school))
    }
}
