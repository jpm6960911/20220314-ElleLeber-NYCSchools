//
//  SATScoreRow.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation
import SwiftUI

struct SATScoreRowData: Identifiable {
    let type: ScoreType
    let score: String?
    
    var id: ScoreType {
        return type
    }
}

struct SATScoreRow: View {
    let data: SATScoreRowData
    
    var body: some View {
        HStack {
            Text(data.type.displayName)
                .font(.system(size: 24, weight: .medium))
            HorizontalLineDivider()
            Text(data.score ?? "--")
                .font(.system(size: 24, weight: .bold))
        } // HStack
    } // body
}
