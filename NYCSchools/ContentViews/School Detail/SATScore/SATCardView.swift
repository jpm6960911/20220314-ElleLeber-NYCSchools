//
//  SATCardView.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation
import SwiftUI

struct SATCardView: View {
    @StateObject var viewModel: SATCardViewModel
        
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("SAT Scores")
                    .font(.title)
                Spacer()
                if case .error = viewModel.state {
                    Button(action: {
                        Task {
                            await viewModel.refresh()
                        }
                    }, label: {
                        Image(systemName: "arrow.counterclockwise.circle")
                            .font(.system(size: 32))
                    })
                }
            } // HStack
            .padding(.bottom, 8)
            ForEach(viewModel.rows) { row in
                SATScoreRow(data: row)
            }
        } // VStack
        .task {
            await viewModel.initialFetch()
        }
    } // body
}
