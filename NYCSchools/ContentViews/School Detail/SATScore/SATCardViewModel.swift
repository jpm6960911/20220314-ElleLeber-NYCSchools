//
//  SATCardViewModel.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation

/// Represents the three distinct SAT score types we care about
enum ScoreType {
    case reading
    case math
    case writing
    
    var displayName: String {
        switch self {
        case .math:
            return "Math"
        case .reading:
            return "Reading"
        case .writing:
            return "Writing"
        }
    }
}

class SATCardViewModel: ObservableObject {
    enum State {
        case idle
        case fetching
        case success(SATScore?)
        case error(Error)
    }
    
    let dbn: String
    let service: NYSchoolDataServiceType
    
    @Published var rows: [SATScoreRowData] = [.init(type: .math, score: nil), .init(type: .reading, score: nil), .init(type: .writing, score: nil)]
    @Published var state: State = .idle
    
    init(dbn: String, service: NYSchoolDataServiceType = NYSchoolDataService()) {
        self.dbn = dbn
        self.service = service
    }
    
    @MainActor
    func refresh() async {
        guard case .error = state else { return }
        await fetchScore()
    }
    
    @MainActor
    func initialFetch() async {
        guard case .idle = state else { return }
        await fetchScore()
    }
    
    @MainActor
    private func fetchScore() async {
        state = .fetching
        do {
            let score = try await service.getSATScore(for: dbn)
            
            rows = [.init(type: .math, score: modify(scoreValueToNumber: score?.satMathAvgScore)), .init(type: .reading, score:  modify(scoreValueToNumber: score?.satCriticalReadingAvgScore)), .init(type: .writing, score:  modify(scoreValueToNumber: score?.satWritingAvgScore))]
            state = .success(score)
        } catch {
            state = .error(error)
        }
    }
    
    private func modify(scoreValueToNumber value: String?) -> String? {
        guard let value = value else { return nil }
        return "\(Int(value) ?? 0)"
    }
}
