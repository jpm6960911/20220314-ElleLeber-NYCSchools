//
//  SchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation
class SchoolDetailViewModel: ObservableObject {
    let school: School
    let service: NYSchoolDataServiceType
    
    @Published var satScore: SATScore?
    
    init(school: School, service: NYSchoolDataServiceType = NYSchoolDataService()) {
        self.school = school
        self.service = service
    }
}
