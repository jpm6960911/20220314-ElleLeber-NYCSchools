//
//  NetworkContainer.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation

// DI Container
class NetworkContainer {
    static let container: NetworkContainer = .init()
    
    lazy var appNetwork: AppNetwork = {
        AppNetwork(session: urlSession, baseUrl: baseUrl, decoder: standardDecoder)
    }()
    
    lazy var urlSession: URLSession = {
        .shared
    }()
    
    lazy var baseUrl: URL = {
        URL(string: "https://data.cityofnewyork.us")!
    }()
    
    lazy var standardDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
}
