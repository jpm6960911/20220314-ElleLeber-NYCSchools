//
//  Common.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation
import SwiftUI

struct Constants {
    // Our NY school data api key. You can replace this with your own if needed.
    static let APIKey = "JTWFKMBrpyIzhL4V9rnfFcQ9Q"
}

extension Color {
    static let defaultBackground = Color("defaultBackground")
    static let defaultNavBar = Color("defaultNavBar")
    static let standardTintColor = Color(red: 125/255, green: 131/255, blue: 255/255)
}

struct HorizontalLineDivider: View {
    var body: some View {
        // Divider() by itself in an HStack will be vertical, this allows us to use a horizontal divider in a HStack
        VStack {
            Divider()
                .background(Color.gray)
                .padding(.horizontal, 12)
        }
    }
}

struct PrimaryNavBarStyle: View {
    var body: some View {
        // iOS since 15+ has an interesting feature where shape / color / materials will affect the navigation bar if you apply them directly under (Y axis) them in the view hierarchy (with no padding).
        // Reference: https://medium.com/@mark.moeykens/how-do-i-customize-the-navigationview-in-swiftui-ef90a530e1aa and https://developer.apple.com/videos/play/wwdc2021/10059/
        Rectangle()
            .frame(height: 0)
            .background(Color.defaultNavBar.opacity(0.2))
    }
}

// MARK: - View modifier for common navigation stack base styling
struct PrimaryNavigationStyleModifier: ViewModifier {
    func body(content: Content) -> some View {
        ZStack {
            Color.defaultBackground
                .ignoresSafeArea()
            VStack(spacing: 0) {
                PrimaryNavBarStyle()
                content
            }
            .padding(0)
        }
    }
}

extension View {
    func styleForNavigationStack() -> some View {
        modifier(PrimaryNavigationStyleModifier())
    }
}

extension Text {
    /// This function expects a Markdown string and will render it as a Text view. If the string is unable to be rendered as valid Markdown then an error Text value will be used instead
    /// - Parameter markdown: This should be a valid mark down string
    init(markdown: String) {
        do {
            let attrString = try AttributedString(markdown: markdown)
            self = Text(attrString)
        } catch {
            assertionFailure("Unable to parse markdown:\(markdown) - Error:\(error)")
            self = Text("ERROR: Invalid markdown")
        }
    }
}
