//
//  NYSchoolDataService.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/15/23.
//

import Foundation

protocol NYSchoolDataServiceType {
    func getAllSchools() async throws -> [School]
    func getSATScore(for dbn: String) async throws -> SATScore?
}
class NYSchoolDataService: NYSchoolDataServiceType {
    let network: AppNetwork

    init(network: AppNetwork = NetworkContainer.container.appNetwork) {
        self.network = network
    }
    func getAllSchools() async throws -> [School] {
        try await network.perform(request: AllSchoolRequest())
    }
    
    func getSATScore(for dbn: String) async throws -> SATScore? {
        try await network.perform(request: SATRequest(dbn: dbn)).first
    }
}
