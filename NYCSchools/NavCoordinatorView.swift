//
//  NavCoordinatorView.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import SwiftUI

enum HomeRoute: Codable, Hashable {
    case school(School)
}

class Coordinator: ObservableObject {
    @Published var path = [HomeRoute]()
    
    func goHome() {
        path.removeAll()
    }
}


struct NavCoordinatorView: View {
    @StateObject var coordinator = Coordinator()
        
    var body: some View {
        NavigationStack(path: $coordinator.path) {
            SchoolListView()
                .styleForNavigationStack()
                .navigationDestination(for: HomeRoute.self) { destination in
                    switch destination {
                    case .school(let detail):
                        SchoolDetailView(viewModel: SchoolDetailViewModel(school: detail))
                            .styleForNavigationStack()
                    }
                }
                .navigationBarTitleDisplayMode(.inline)
        } // NavigationStack
        .tint(Color.standardTintColor)
        .environment(\.openURL, OpenURLAction { url in
                .systemAction
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavCoordinatorView()
    }
}
