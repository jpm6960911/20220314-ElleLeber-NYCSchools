//
//  SATScore.swift
//  NYCSchools
//
//  Created by Leber, Elle on 3/14/23.
//

import Foundation

struct SATScore: Codable {
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
}
