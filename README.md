# NY School App
This is a simple application that consumes some data from the NY school API here https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2 it also fetches SAT average scores from here https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4

The app is simple and has two screens, one is a list view of all schools, tapping on a row will take you to a detail screen with some additional information. This includes contact and address information. Tapping on links will launch the correspodning iOS app. Ttapping directions will launch the apple maps app with directions to the school, tapping the website URL will take the user to safar, and tapping email or phone links will launch either the mail app or phone app.

## Structure
The app is constructed completely in SwiftUI using async await and a view model architecture. The view model is the bridge between the network layer and the view layer, handling state business logic as well. (i.e. fetching, error, success)

Errors are handled via the UI with a "retry" button that will re attempt the request. The detail school view also has this for the SAT score portion. If the SAT portion fails a retry button will appear above the module.
