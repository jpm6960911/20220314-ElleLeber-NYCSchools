//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Leber, Elle on 3/14/23.
//

import XCTest
@testable import NYCSchools

class FakeNYSchoolDataService: NYSchoolDataServiceType {
    var stubGetAllSchoolsResult: Result<[School], Error>?
    var stubGetSATScoreResult: Result<SATScore, Error>?
    
    func getAllSchools() async throws -> [NYCSchools.School] {
        try stubGetAllSchoolsResult?.get() ?? []
    }
    
    func getSATScore(for dbn: String) async throws -> NYCSchools.SATScore? {
        try stubGetSATScoreResult?.get()
    }
}

enum TestError: Error {
    case some
}

import Nimble

final class NYCSchoolsTests: XCTestCase {
    var service: FakeNYSchoolDataService!
    var subject: SchoolListViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        service = .init()
        subject = .init(service: service)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // Example of how to test the viewmodel -> server response interactions
    func testHomeFetchesSuccess() async {
        service.stubGetAllSchoolsResult = .success([.init(schoolName: "Name", dbn: "1234", totalStudents: "", overviewParagraph: "", primaryAddressLine1: "", city: "", zip: "", stateCode: "", schoolEmail: "", phoneNumber: "", faxNumber: "", website: "", latitude: "", longitude: "")])
        
        await subject.initialFetch()
        guard case let .success(results) = subject.state else {
            fail("Not correct state")
            return
        }
        expect(results.first?.id).to(equal("1234"))
    }
    
    func testHomeFetchesError() async {
        service.stubGetAllSchoolsResult = .failure(TestError.some)
        
        await subject.initialFetch()
        
        guard case let .error(error) = subject.state else {
            fail("Not correct state")
            return
        }
        expect(error).to(matchError(TestError.some))
    }
}

class SATCardViewModelTests: XCTestCase {
    var service: FakeNYSchoolDataService!
    var subject: SATCardViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        service = .init()
        subject = SATCardViewModel(dbn: "1234", service: service)
    }
    
    func testHandlesForNonNumberScoreValues() async {
        service.stubGetSATScoreResult = .success(.init(dbn: "1234", schoolName: "", numOfSatTestTakers: "", satCriticalReadingAvgScore: "s", satMathAvgScore: "s", satWritingAvgScore: "s"))
        
        await subject.initialFetch()
        
        XCTAssertEqual(subject.rows[0].score, "0")
        XCTAssertEqual(subject.rows[1].score, "0")
        
    }
}
